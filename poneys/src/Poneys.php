<?php 
const QT_INITIAL = 8;
const TAILLECHAMP = 15;
  class Poneys {
      private $count = QT_INITIAL;

      public function getCount() {
        return $this->count;
      }

      public function setCount($value){
        $this->count=$value;
      }

      public function addPoneyToField($number){
        $this->count +=$number;
      }

      public function removePoneyFromField($number) {
        if ($number >$this->count) {
          throw new Exception("Vous essayez de retirer plus de poneys que disponibles");
        }
        $this->count -= $number;
      }

      public function getNames() {

      }

      public function hasPlacesLeft(){
        return ($this->count < TAILLECHAMP);
      }
  }
?>
