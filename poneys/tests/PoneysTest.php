<?php
  require_once 'src/Poneys.php';
  use PHPUnit\Framework\TestCase;

  class PoneysTest extends TestCase {

    private $Poneys;

    protected function setUp(){
      $this->Poneys=new Poneys();
      $this->Poneys->setCount(QT_INITIAL);
    }

    /**
     * Test avec DataProvider
     * 
     * @dataProvider remove_DataProvider
     */
    public function test_removePoneyFromField($nombreASupprimer,$nombreRestant) {
      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());
    }
    //Le DataProvider
    public function remove_DataProvider(){
      return [[1,QT_INITIAL-1],
              [2,QT_INITIAL-2],
              [3,QT_INITIAL-3],
              [4,QT_INITIAL-4]];
    }

    //Test simple d'ajout de poneys
    public function test_addPoneyFromField(){
      $this->Poneys->addPoneyToField(3);
      $this->assertEquals(QT_INITIAL+3, $this->Poneys->getCount());
    }

    /**
     * Test d'exeption en cas d'un trop grand nombre de poneys enlevés
     * @expectedException Exception
     */
    public function test_ExceptionRemove(){
      $this->Poneys->removePoneyFromField(QT_INITIAL+1);
    }

    //Test avec un mock
    public function test_mockedPoneys(){
      $MockedPoneys = $this->getMockBuilder(Poneys::class)
                     ->setMethods(["getNames"])
                     ->getMock();
      

      $MockedPoneys->expects($this->once())->method("getNames")->willReturn(["Poney1","Poney2","Poney3","Poney4"]);
      $this->assertEquals($MockedPoneys->getNames(),["Poney1","Poney2","Poney3","Poney4"]);
    }

    //Test vrai/faux
    public function test_hasPlacesLeft(){
      $this->assertTrue($this->Poneys->hasPlacesLeft());
      $this->Poneys->addPoneyToField(TAILLECHAMP-QT_INITIAL);
      $this->assertFalse($this->Poneys->hasPlacesLeft());      
    }









    protected function tearDown(){
      unset($this->Poneys);
    }
  }
 ?>
