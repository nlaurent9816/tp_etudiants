<?php
    require_once 'src/Poneys.php';
    use PHPUnit\Framework\TestCase;

    class PoneysTest2 extends TestCase {

        /**
         * @dataProvider count_DataProvider
         */
        public function test_count($count){
            $Poneys = new Poneys();
            $Poneys->setCount($count);
            $this->assertEquals($count,$Poneys->getCount());
        }

        public function count_DataProvider(){
            return [[5],[1],[12]];
        }
    }
?>