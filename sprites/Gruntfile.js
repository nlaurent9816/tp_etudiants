module.exports = function(grunt) {
    
      // Project configuration.
      grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sprite:{
            all: {
              src: 'img/*.png',
              dest: 'dist/spritesheet.png',
              destCss: 'dist/sprites.css'
            }
          }
      });
    
      // Load the plugin that provides the "uglify" task.
      grunt.loadNpmTasks('grunt-spritesmith');
    
      // Default task(s).
      grunt.registerTask('default', ['sprite']);
    
    };